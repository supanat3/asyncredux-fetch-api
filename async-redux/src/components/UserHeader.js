import React, {useEffect} from "react"
import { connect } from "react-redux"
import { fetchUser } from "../action"
const UserHeader = (props) => {
console.log("🚀 ~ file: UserHeader.js ~ line 5 ~ UserHeader ~ props", props)
    useEffect(() => {
        props.fetchUser(props.userId)
    },[])
    const userName = props.user?.find(user => user.id === props.userId).name
    return(
        <div>{userName}</div>
    )
}

const mapStateToProps = (state) => {
// console.log("🚀 ~ file: UserHeader.js ~ line 14 ~ mapStateToProps ~ state", state)
    return {user: state.user}
}

export default connect(mapStateToProps ,{fetchUser})(UserHeader)